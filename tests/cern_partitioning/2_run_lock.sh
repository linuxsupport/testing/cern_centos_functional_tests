#!/bin/bash
# Author: Alex Iribarren <Alex.Iribarren@cern.ch>
#

t_Log "Running $0 - Checking /run/lock"

t_Log "Running $0 - Checking that /run/lock exists and is a directory"
[[ -d /run/lock ]]
t_CheckExitStatus $?

t_Log "Running $0 - Checking that /run/lock/subsys exists and is a directory"
[[ -d /run/lock/subsys ]]
t_CheckExitStatus $?
