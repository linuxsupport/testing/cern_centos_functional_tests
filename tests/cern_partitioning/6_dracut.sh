#!/bin/bash
# Author: Alex Iribarren <Alex.Iribarren@cern.ch>
#

t_Log "Running $0 - dracut checks"

t_Log "Checking if cern-dracut-conf is installed"
rpm -q cern-dracut-conf
t_CheckExitStatus $?

t_Log "Checking contents of dracut config files"
cat /etc/dracut.conf.d/*
t_CheckExitStatus $?
