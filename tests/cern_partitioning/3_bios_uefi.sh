#!/bin/bash
# Author: Alex Iribarren <Alex.Iribarren@cern.ch>
#

t_Log "Running $0 - Checking if booted from BIOS or UEFI"

if [[ -d /sys/firmware/efi ]]; then
    t_Log "Booted in UEFI mode"
else
    t_Log "Booted in BIOS mode"
fi
t_CheckExitStatus $?
