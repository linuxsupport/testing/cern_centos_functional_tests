#!/bin/bash
# Author: Alex Iribarren <Alex.Iribarren@cern.ch>
#

TMP=`mktemp`

t_Log "Running $0 - List initrd modules"
/usr/bin/lsinitrd > $TMP
t_CheckExitStatus $?

# We need mdraid in initrd for Ironic installs on RAID devices
t_Log "Running $0 - Check if mdraid module is present"
grep -wq mdraid $TMP
t_CheckExitStatus $?

if [ "$centos_ver" -ge 8 ] ; then
  t_Log "Running $0 - Check if dm module is present"
  grep -wq dm $TMP
  t_CheckExitStatus $?
fi

DRIVERS="raid0 raid1 raid10 raid456 linear nvme"
for driver in $DRIVERS; do
  t_Log "Running $0 - Check if driver $driver is present"
  grep -q "${driver}.ko.xz$" $TMP
  t_CheckExitStatus $?
done

rm -rf $TMP
