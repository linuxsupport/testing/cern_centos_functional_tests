#!/bin/bash
# Author: Alex Iribarren <Alex.Iribarren@cern.ch>
#

t_Log "Running $0 - Checking default grub parameters"

t_Log "Looking for crashkernel in /etc/default/grub"
grep -e 'GRUB_CMDLINE_LINUX=".*crashkernel.*' /etc/default/grub
t_CheckExitStatus $?

# el9 onwards cannot use crashkernel=auto
# Ref. https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9-beta/html/managing_monitoring_and_updating_the_kernel/configuring-kdump-on-the-command-line_managing-monitoring-and-updating-the-kernel
if [ "$centos_ver" -ge 9 ] ; then
  t_Log "Making sure crashkernel=auto is not in /etc/default/grub"
  ! grep -e 'GRUB_CMDLINE_LINUX=".*crashkernel=auto.*' /etc/default/grub
  t_CheckExitStatus $?
fi

t_Log "Looking for crashkernel in /proc/cmdline"
grep -e 'crashkernel' /proc/cmdline
t_CheckExitStatus $?

t_Log "Looking for rd.auto in /etc/default/grub"
grep -e 'GRUB_CMDLINE_LINUX=".*rd.auto.*' /etc/default/grub
t_CheckExitStatus $?
t_Log "Looking for rd.auto in /proc/cmdline"
grep -e 'rd.auto' /proc/cmdline
t_CheckExitStatus $?

t_Log "Looking for net.ifnames=0 in /etc/default/grub"
grep -e 'GRUB_CMDLINE_LINUX=".*net.ifnames=0.*' /etc/default/grub
t_CheckExitStatus $?
t_Log "Looking for net.ifnames=0 in /proc/cmdline"
grep -e 'net.ifnames=0' /proc/cmdline
t_CheckExitStatus $?

if [ "$centos_ver" -ge 8 ] ; then
  t_Log "Looking for root UUID in /etc/default/grub"
  grep -e 'GRUB_CMDLINE_LINUX=".*root=UUID=.*' /etc/default/grub
  t_CheckExitStatus $?
  t_Log "Looking for root UUID in /proc/cmdline"
  grep -e 'root=UUID=' /proc/cmdline
  t_CheckExitStatus $?
fi
