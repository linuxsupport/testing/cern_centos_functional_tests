#!/bin/bash
# Author: Daniel Juarez <djuarezg@cern.ch>
#

t_Log "Running $0 - Checking partition labels"

t_Log "Running $0 - Checking we have ROOT part label"
blkid | grep -q 'LABEL="ROOT"'
t_CheckExitStatus $?

t_Log "Running $0 - Checking we have EFI part label"
blkid | grep -q 'LABEL="EFI"'
t_CheckExitStatus $?
