#!/bin/bash
# Author: Daniel Juarez <djuarezg@cern.ch>
#

t_Log "Running $0 - Setting up AFS"

t_InstallPackage openafs-client
t_InstallPackage openafs-krb5
t_InstallPackage cern-wrappers

t_ServiceControl openafs-client start

t_Log "Running $0 - checking that the callback port is working"
rxdebug localhost 7001 -version
t_CheckExitStatus $?
