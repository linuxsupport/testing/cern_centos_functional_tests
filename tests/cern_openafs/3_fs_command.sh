#!/bin/bash
# Author: Jan Iven <Jan.Iven@cern.ch>
#

t_Log "Running $0 - Checking AFS 'fs' commands."

AFSDIR="/afs/cern.ch/user/${IMAGECI_USER:0:1}/${IMAGECI_USER}"

t_Log "Running $0 - 'fs wscell'"
fs wscell | grep 'cern.ch'
t_CheckExitStatus $?

t_Log "Running $0 - 'fs getcrypt'"
fs getcrypt | grep 'level is currently crypt'
t_CheckExitStatus $?

t_Log "Running $0 - 'fs whereis'"
fs whereis -path "${AFSDIR}"
t_CheckExitStatus $?

t_Log "Running $0 - 'fs examine'"
fs examine -path "${AFSDIR}" | grep "named user\.${IMAGECI_USER}"
t_CheckExitStatus $?

t_Log "Running $0 - 'fs listquota'"
fs listquota -path "${AFSDIR}" | grep "user\.${IMAGECI_USER}"
t_CheckExitStatus $?

t_Log "Running $0 - 'fs listacl'"
fs listacl -path "${AFSDIR}" -cmd | grep "${IMAGECI_USER} rlidwka"
t_CheckExitStatus $?

# the following will change state
t_Log "Running $0 - 'fs setacl', remove system:administrators"
fs setacl -dir "${AFSDIR}" -acl system:administrators ''
t_CheckExitStatus $?

t_Log "Running $0 - 'fs setacl', re-add system:administrators"
fs setacl -dir "${AFSDIR}" -acl system:administrators rlidwka
t_CheckExitStatus $?
