#!/bin/bash
# Author: Jan Iven <Jan.Iven@cern.ch>
#

t_Log "Running $0 - Checking AFS 'vos' commands."

AFSVOL="user.${IMAGECI_USER}"

t_Log "Running $0 - 'vos listvdlb -name ${AFSVOL}'"
vos listvldb -name "${AFSVOL}"
t_CheckExitStatus $?

t_Log "Running $0 - 'vos examine -id ${AFSVOL}'"
vos examine -id "${AFSVOL}"
t_CheckExitStatus $?
