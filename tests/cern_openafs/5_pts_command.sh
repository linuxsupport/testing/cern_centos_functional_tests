#!/bin/bash
# Author: Jan Iven <Jan.Iven@cern.ch>
#

t_Log "Running $0 - Checking AFS 'pts' commands."

AFSUSERVOL="user.${IMAGECI_USER}"

t_Log "Running $0 - 'pts examine -nameorid ${IMAGECI_USER}'"
pts examine -nameorid "${IMAGECI_USER}"
t_CheckExitStatus $?

t_Log "Running $0 - 'pts membership -nameorid ${IMAGECI_USER}'"
pts membership -nameorid "${IMAGECI_USER}" | grep 'is a member of'
t_CheckExitStatus $?

t_Log "Running $0 - 'pts listowned -nameorid ${IMAGECI_USER}'"
pts listowned -nameorid "${IMAGECI_USER}" | grep 'Groups owned'
t_CheckExitStatus $?
