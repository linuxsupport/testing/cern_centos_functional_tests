#!/bin/bash
# Author: Jan Iven <Jan.Iven@cern.ch>
#

t_Log "Running $0 - Checking that the AFS filesystem works"

### filesystem-level tests
AFSDIR="/afs/cern.ch/user/${IMAGECI_USER:0:1}/${IMAGECI_USER}"

t_Log "Running $0 - Checking we can list ${AFSDIR}"
ls "${AFSDIR}"
t_CheckExitStatus $?

t_Log "Running $0 - Checking we can create a file"
tmpfile=$(mktemp "${AFSDIR}/ci_tmp_XXXXXXX")
t_CheckExitStatus $?

t_Log "Running $0 - Checking we can write to ${tmpfile}"
echo "Test content:$tmpfile" > "${tmpfile}"
t_CheckExitStatus $?

t_Log "Running $0 - Checking we can stat ${tmpfile}"
stat "${tmpfile}"
t_CheckExitStatus $?

t_Log "Running $0 - Checking we can list ${tmpfile}"
ls -l "${AFSDIR}" | grep "${tmpfile##*/}"
t_CheckExitStatus $?

t_Log "Running $0 - Checking we can read from ${tmpfile}"
grep "Test content:$tmpfile" "${tmpfile}"
t_CheckExitStatus $?

t_Log "Running $0 - Checking we can remove ${tmpfile}"
rm "${tmpfile}"
t_CheckExitStatus $?
