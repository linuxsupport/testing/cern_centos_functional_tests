#!/bin/bash
# Author: Jan Iven <Jan.Iven@cern.ch>
#

t_Log "Running $0 - getting (Kerberos+)AFS token"
echo "${IMAGECI_PWD}" | /usr/sue/bin/kinit "${IMAGECI_USER}@CERN.CH"
t_CheckExitStatus $?

t_Log "Running $0 - checking the AFS token"
tokens | grep '[tT]okens for .*cern.ch'
t_CheckExitStatus $?
