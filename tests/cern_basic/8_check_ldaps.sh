#!/bin/bash
# Author: Alex Iribarren <Alex.Iribarren@cern.ch>

t_Log "Running $0 - Installing openldap-clients"

t_InstallPackage openldap-clients

t_Log "Running $0 - querying LDAPS server for user ${IMAGECI_USER}"

ldapsearch -LLL -x \
    -b "DC=cern,DC=ch" \
    -H ldaps://cerndc.cern.ch \
    -D "CN=${IMAGECI_USER},OU=Users,OU=Organic Units,DC=cern,DC=ch" \
    -w "${IMAGECI_PWD}" \
    "(&(objectclass=user)(name=${IMAGECI_USER}))" userPrincipalName

t_CheckExitStatus $?
