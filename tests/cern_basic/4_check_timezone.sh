#!/bin/sh
# Author: Alex Iribarren <Alex.Iribarren@cern.ch>

t_Log "Running $0 - Checking timezone"

[[ "`readlink -f /etc/localtime`" == "/usr/share/zoneinfo/Europe/Zurich" ]]
t_CheckExitStatus $?
