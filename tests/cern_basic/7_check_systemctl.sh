#!/bin/sh
# Author: Ben Morrice <ben.morrice@cern.ch>
# Reference: INC3028909

t_Log "Running $0 - Checking for systemctl: failed states / NeedDaemonReload / masked units"

# Check for https://github.com/cernops/collectd-systemd#systemd-systemd-stateboolean-running
systemctl --failed &>/dev/null
if [ $? -ne 0 ]; then
  t_CheckExitStatus $?
fi

# Check for https://github.com/cernops/collectd-systemd#systemd-needreloadboolean-needdaemonreload
NEED_DAEMON_RELOAD=0
MASKED_UNITS=0
# Ensure we avoid parsing the 'colored dot' that systemctl can append
# to services under certain criteria
for UNIT in $(LC_CTYPE=C systemctl --all --no-pager --no-legend | cut -d\* -f2 | awk '{print $1}'); do
  CHECK_RELOAD=$(systemctl show -p NeedDaemonReload -- $UNIT | grep -q yes)
  if [ $? -eq 0 ]; then
    echo "$UNIT NeedDaemonReload"
    NEED_DAEMON_RELOAD=1
  fi
  CHECK_MASK=$(systemctl show -p LoadState -- $UNIT | grep -q masked)
  if [ $? -eq 0 ]; then
    echo "$UNIT is masked"
    MASKED_UNITS=1
  fi
done

if [ $NEED_DAEMON_RELOAD -ne 0 ]; then
  t_CheckExitStatus $NEED_DAEMON_RELOAD
fi

t_CheckExitStatus $MASKED_UNITS
