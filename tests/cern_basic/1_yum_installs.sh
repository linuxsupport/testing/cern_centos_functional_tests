#!/bin/bash
# Author: Daniel Juarez <djuarezg@cern.ch>
#

t_Log "Running $0 - Running yum install for some cern packages to check if they install"

# Not all packages are yet available for c8
if [ "$centos_ver" -ge 8 ] ; then
  yum install cern-linuxsupport-access useraddcern cern-wrappers krb5-workstation -y &> /dev/null
else
  yum install cvmfs cvmfs-config cern-linuxsupport-access openafs-krb5 phonebook useraddcern cern-wrappers krb5-workstation -y &> /dev/null
fi

t_CheckExitStatus $?
