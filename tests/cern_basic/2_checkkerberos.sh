#!/bin/bash 
# Author: Daniel Juarez <djuarezg@cern.ch>
#

t_Log "Running $0 - creating a valid Kerberos ticket"

echo "${IMAGECI_PWD}" | kinit ${IMAGECI_USER}@CERN.CH

t_CheckExitStatus $?

t_Log "Running $0 - testing CERN's host kerberos principal"

klist | grep "krbtgt/CERN.CH@CERN.CH" &> /dev/null

t_CheckExitStatus $?

