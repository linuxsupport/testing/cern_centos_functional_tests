#!/bin/sh
# Author: Alex Iribarren <Alex.Iribarren@cern.ch>

t_Log "Running $0 - Checking BUILDTIME"

if [ -f /etc/.BUILDTIME ]; then
  echo "Old /etc/.BUILDTIME found, should be deprecated:"
  cat /etc/.BUILDTIME
  t_CheckExitStatus 1
fi

if [ -f /etc/BUILDTIME ]; then
  echo "New /etc/BUILDTIME found:"
  cat /etc/BUILDTIME
else
  echo "No /etc/BUILDTIME found, must not be an image install."
fi

t_CheckExitStatus 0