#!/bin/sh
# Author: Daniel Juarez <djuarezg@cern.ch>
# Ref.: http://bugs.centos.org/view.php?id=4943

if [ "$centos_ver" -ge 8 ] ; then
  exit 0
fi
t_Log "Running $0 - NTP is using CERN CentOS server pool test."

grep ".cern.ch" /etc/ntp.conf > /dev/null 2>&1

t_CheckExitStatus $?

