#!/bin/sh
# Author: Ben Morrice <ben.morrice@cern.ch>

t_Log "Running $0 - Checking ipv6"

tryPing6() {
  COUNT=0
  while [ $COUNT -lt 3 ]; do
    ping6 -c 1 $1
    RET=$?
    if [ $RET -eq 0 ]; then
      return 0
    fi
    COUNT=$(($COUNT + 1))
    sleep 10s
  done
  return $RET
}

if [ -f /proc/net/if_inet6 ]; then
  echo "Running kernel is IPv6 ready, running ping6 to cern.ch"
  tryPing6 cern.ch
  RET=$?
  if [ $RET -ne 0 ]; then
    tryPing6 linuxsoft.cern.ch
    RET=$?
    if [ $RET -ne 0 ]; then
      tryPing6 home.cern
      RET=$?
    fi
  fi
else
  echo "Running kernel does not support IPv6"
  RET=0
fi
t_CheckExitStatus $RET
