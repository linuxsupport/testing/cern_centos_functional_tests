This project is a reduced version of <https://gitlab.cern.ch/linuxsupport/testing/centos_functional_tests> with CERN specific tests.

Add them on `./tests/cern_specific/`.

## Running tests

To run these tests on your local machine :
```
./runtests.sh
```

To only run a specific test ( eg. p_openssh ) :
```
./runtests.sh p_openssh
```
## Writing tests

There is a dedicated [wiki page](http://wiki.centos.org/QaWiki/AutomatedTests/WritingTests/t_functional) covering that. As a newcomer, you should read this document from start to finish.
Questions/comments/suggestions should be voiced to `linux-experts@cern.ch` or on [our Linux Mattermost channel](https://mattermost.web.cern.ch/it-dep/channels/linux).

Feel free to propose Merge Requests to add your own tests if they are useful for the entire community.

## Disabling tests

While it's a very bad idea, sometimes, during major.minor release, our scripts really find issues that are then reported upstream.
For the time being, one can add tests to be skipped by our QA harness setup (validating all new installable trees)

See the [skipped-tests.list](skipped-tests.list) file.
